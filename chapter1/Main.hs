module Main where

-- All types could be inferred, but I included them for the sake of exercising the ability to identify types

funcA :: IO (Int)  
funcA = return 1

funcB :: IO (Char) 
funcB = return  'A'

doThing :: IO ()
doThing = putStrLn "Doing a thing"

combine :: IO a -> IO b -> IO b -- order of the functions matter for this type. Might be best to leave for type inference
combine before after = do
  before
  putStrLn "Middle"
  after

doStuff :: IO ()
doStuff = do
  a <- funcA
  b <- funcB
  print a
  print b
  putStrLn "STUFF"

process :: IO ()
process = do putStrLn "Doing some stuff soon"
             doStuff
             putStrLn "Done stuff"

getName :: IO [Char] 
-- getName = do
--   putStrLn "What is your name?"
--   getLine
getName = putStrLn "What is your name?" >> getLine 

greet :: [Char] -> IO ()
greet person = putStrLn $ "Hello, " ++ person

introduce :: IO ()
introduce = getName >>= greet

askFavoriteColor :: IO ()
-- askFavoriteColor = do
--   putStrLn "What is your favorite color?"
--   color <- getLine
--   putStrLn ("Ah, your favorite color is " ++ color ++ ". Nice.")
askFavoriteColor = putStrLn "What is your favorite color?" 
                   >> getLine 
                   >>= putStrLn . (\color -> "Ah, your favorite color is " ++ color ++ ". Nice.")


main :: IO ()
main = do
  ---- Exercise
  -- input <- getContents
  -- putStrLn ("DEBUG: got input " ++ input)

  ---- Exercise
  -- process

  ---- Exercise
  -- combine doThing doThing 
  -- let b = combine funcA funcB 
  -- let d = combine b (combine process process) 
  -- putStrLn "Dooone"

  ---- Exercise
  -- getName >>= greet
  -- introduce

  ---- Exercise
  introduce
  askFavoriteColor
