{-# LANGUAGE FlexibleContexts #-}
module Main where

import Text.ParserCombinators.Parsec
import Control.Monad
import Control.Applicative hiding (many)


parseInput = do
  dirs <- many dirAndSize
  eof :: Parser ()
  return dirs

data Dir = Dir Int String deriving Show
-- data Dir = D Int String deriving Show ---> Dir datatype, D data constructor

dirAndSize = do
  size <- many1 digit
  spaces
  dir_name <- anyChar `manyTill` newline
  return (Dir (read size) dir_name)

-- Exercise: examine types of "digit", "anyChar", "many", "many1", "manyTill"
-- digit :: Text.Parsec.Prim.Stream s m Char => Text.Parsec.Prim.ParsecT s u m Char
-- Parses a digit. Returns the parsed character

-- anyChar :: 

--https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/parsing-floats-with-parsec 
readInteger = rd <$> many1 digit
  where rd = read :: String -> Integer

main :: IO ()
main = do
  putStrLn "Welcome to Chapter 2"
  putStrLn "Enter an Integer" >> getLine >>= parseTest readInteger 

  putStrLn "Enter an Integer and a directory name" >> getLine >>= parseTest parseInput 
